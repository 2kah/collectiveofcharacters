﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AssemblyCSharp;

public class CharacterSelect : MonoBehaviour {

	public Texture2D BackgroundTexture;
	public Texture2D DPSTexture;
	public Texture2D MageTexture;
	public Texture2D SupportTexture;
	public Texture2D TankTexture;
	public GUIStyle MenuStyle;
	public GUIStyle ButtonStyle;

	string characterDescription = "{0}\nPrimary stat: {1}\nDescription: {2}";
	string dpsDescription;
	string mageDescription;
	string supportDescription;
	string tankDescription;

	CharacterFactory characterFactory;

	Character[] allCharacters;
	Character[] selectedCharacters;

	// Use this for initialization
	void Start ()
	{
		characterFactory = GameObject.Find("CharacterFactory").GetComponent<CharacterFactory>();
		allCharacters = new Character[4];
		allCharacters[0] = new Character
		{
			Description = string.Format(characterDescription,
			                            "DPS",
			                            "Damage",
			                            "A DPS can upgrade the damage they inflict per second while in combat. This makes them good during sustained fights and strong objective pushers."
			                            ),
			Tex = DPSTexture,
			Class = CharacterFactory.CharacterClass.DPS
		};
		allCharacters[1] = new Character
		{
			Description = string.Format(characterDescription,
			                            "Mage",
			                            "Magic",
			                            "A Mage can upgrade their magic which is a large amount of damage done instantly which then goes on cooldown. Magic damage does not affect objectives. Mages are good at quickly killing low health characters."
			                            ),
			Tex = MageTexture,
			Class = CharacterFactory.CharacterClass.Mage
		};
		allCharacters[2] = new Character
		{
			Description = string.Format(characterDescription,
			                            "Support",
			                            "Buff",
			                            "A Support can upgrade their buff which improves the stats of allies while they are in the same location. Supports are weak on their own and so should be kept with another character, which they will aid in both offense and defense."
			                            ),
			Tex = SupportTexture,
			Class = CharacterFactory.CharacterClass.Support
		};
		allCharacters[3] = new Character
		{
			Description = string.Format(characterDescription,
			                            "Tank",
			                            "Max Health",
			                            "A Tank can upgrade their maximum health, allowing them to take tons of damage before dying. They are good at protecting other characters."
			                            ),
			Tex = TankTexture,
			Class = CharacterFactory.CharacterClass.Tank
		};

		selectedCharacters = new Character[5];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), BackgroundTexture);
		GUI.Label(new Rect(Screen.width/4, 80, Screen.width/2, 20), "Select your 5 characters", MenuStyle);

		for(int i = 0; i < 4; i++)
		{
			if(GUI.Button(new Rect(270+(120*i), 150, 60, 60), new GUIContent(allCharacters[i].Tex, allCharacters[i].Description), ButtonStyle))
			{
				selectedCharacters[NullIndex(selectedCharacters)] = allCharacters[i];
			}
		}
		GUI.Label(new Rect(270, 230, 420, 200), GUI.tooltip);

		for(int i = 0; i < 5; i++)
		{
			if(GUI.Button(new Rect(210+(120*i), 400, 60, 60), selectedCharacters[i] == null ? null : selectedCharacters[i].Tex))
			{
				selectedCharacters[i] = null;
			}
		}

		if(GUI.Button(new Rect(440, 500, 80, 40), "Lock In"))
		{
			if(ReadyToStart())
			{
				CreateCharacters();
				Destroy (this);
			}
		}
	}

	void CreateCharacters()
	{
		//pick 5 random characters for yellow, but ensure no character is picked more than twice
		var availableCharacters = new List<Character>(allCharacters);
		var characterCounts = new List<int>(new int[] {0, 0, 0, 0});
		var yellowCharacters = new Character[5];
		for(int i = 0; i < 5; i++)
		{
			int selectedIndex = Random.Range(0,availableCharacters.Count);
			yellowCharacters[i] = availableCharacters[selectedIndex];
			characterCounts[selectedIndex]++;
			if(characterCounts[selectedIndex] >= 2)
			{
				availableCharacters.RemoveAt(selectedIndex);
				characterCounts.RemoveAt(selectedIndex);
			}
		}
		foreach(var selectedYellow in yellowCharacters)
		{
			characterFactory.Create(selectedYellow.Class, false);
        }

		//create the blue selected characters
		foreach(var selected in selectedCharacters)
		{
			characterFactory.Create(selected.Class, true);
		}
	}

	int NullIndex(Character[] array)
	{
		for(int i = 0; i < array.Length; i++)
		{
			if(array[i] == null) return i;
		};
		return array.Length - 1;
	}

	bool ReadyToStart()
	{
		for(int i = 0; i < selectedCharacters.Length; i++)
		{
			if(selectedCharacters[i] == null) return false;
		}
		return true;
	}

	class Character
	{
		public string Description;
		public Texture2D Tex;
		public CharacterFactory.CharacterClass Class;
	}
}
