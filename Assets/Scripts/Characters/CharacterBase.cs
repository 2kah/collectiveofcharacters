﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CharacterBase : MonoBehaviour, IHasHealth {

	public bool IsAlive = true;
	public bool IsBlue;
	public int Health = 100;
	public float MaxHealth = 100;
	public float Damage = 10;
	public float Magic = 0;
	public float Buff = 0;

	public LocationBase CurrentLocation;
	public Transform BlueBackground;
	public Transform YellowBackground;
	public Transform SelectedBackground;

	Transform currentBackground;
	Player player;

	// Use this for initialization
	public void Initialise ()
	{
		player = GameObject.Find("Player").GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseUpAsButton()
	{
		//let player object know this character has been clicked
		player.CharacterClicked(this);
	}

	public virtual void IncreasePrimaryStat(float amount)
	{}

	public int GetCurrentHealth()
	{
		return Health;
	}

	public int GetMaxHealth()
	{
		return Mathf.RoundToInt(MaxHealth);
	}

	public void SetBackground(bool isSelected)
	{
		if(currentBackground != null) Destroy(currentBackground.gameObject);
		var newBackground = IsBlue ? BlueBackground : YellowBackground;
		if(isSelected) newBackground = SelectedBackground;
		currentBackground = (Transform) Instantiate(newBackground);
		currentBackground.position = this.transform.position;
		currentBackground.parent = this.transform;
	}

	public void MoveToLocation(LocationBase location)
	{
		//remove self from current location list
		if(CurrentLocation != null)
		{
			var oldList = IsBlue ? CurrentLocation.BlueCharactersPresent : CurrentLocation.YellowCharactersPresent;
			oldList.Remove(this);
			//tell all the characters in the old location to update their position
			foreach(var character in oldList)
			{
                character.UpdatePosition();
			}
		}

		//update current location
		CurrentLocation = location;
		//add self to new location list
        var newList = IsBlue ? CurrentLocation.BlueCharactersPresent : CurrentLocation.YellowCharactersPresent;
		newList.Add(this);
		//tell all the characters in the new location to update their position
		foreach(var character in newList)
		{
			character.UpdatePosition();
        }
	}

	public void UpdatePosition()
	{
		//work out where to render
		var charList = IsBlue ? CurrentLocation.BlueCharactersPresent : CurrentLocation.YellowCharactersPresent;
		var index = charList.IndexOf(this);
		var total = charList.Count;

		//actual formula is 0.25i - ((t-1)-i)0.25
		var spacing = 0.3f;
		var posCalc = ((total - 1) - index) * spacing;
		var newX = CurrentLocation.transform.position.x + (spacing * index) - posCalc;

		var newY = CurrentLocation.transform.position.y + (IsBlue ? - 0.5f : 0.5f);
		transform.position = new Vector3(newX, newY);
	}
}
