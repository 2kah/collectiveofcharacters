﻿using UnityEngine;
using System.Collections;

public class CharacterFactory : MonoBehaviour {

	public enum CharacterClass {DPS, Mage, Support, Tank};

	public Transform DPSPrefab;
	public Transform MagePrefab;
	public Transform SupportPrefab;
	public Transform TankPrefab;

	public LocationBase BlueSpawn;
	public LocationBase YellowSpawn;

	// Use this for initialization
	void Start ()
	{
		BlueSpawn = GameObject.Find("BlueBase").GetComponent<LocationBase>();
		YellowSpawn = GameObject.Find("YellowBase").GetComponent<LocationBase>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Transform Create(CharacterClass charClass, bool isBlue)
	{
		Transform character;
		switch(charClass)
		{
		case CharacterClass.DPS:
			character = (Transform) Instantiate(DPSPrefab);
			break;
		case CharacterClass.Mage:
			character = (Transform) Instantiate(MagePrefab);
			break;
		case CharacterClass.Support:
			character = (Transform) Instantiate(SupportPrefab);
			break;
		case CharacterClass.Tank:
			character = (Transform) Instantiate(TankPrefab);
			break;
		default:
			//should never happen, is this even possible?
			return null;
		}

		var charObject = character.GetComponent<CharacterBase>();
		charObject.IsBlue = isBlue;
		charObject.MoveToLocation(isBlue ? BlueSpawn : YellowSpawn);
		charObject.SetBackground(false);
		charObject.Initialise();

		return character;
	}
}
