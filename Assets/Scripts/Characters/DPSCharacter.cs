﻿using UnityEngine;
using System.Collections;

public class DPSCharacter : CharacterBase {

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void IncreasePrimaryStat(float amount)
	{
		this.Damage += amount;
	}
}
