﻿using UnityEngine;
using System.Collections;

public class MageCharacter : CharacterBase {

	// Use this for initialization
	void Start ()
	{
		Magic = 50;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void IncreasePrimaryStat(float amount)
	{
		this.Magic += amount;
	}
}
