﻿using UnityEngine;
using System.Collections;

public class SupportCharacter : CharacterBase {

	// Use this for initialization
	void Start ()
	{
		Buff = 5;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void IncreasePrimaryStat(float amount)
	{
		this.Buff += amount;
	}
}
