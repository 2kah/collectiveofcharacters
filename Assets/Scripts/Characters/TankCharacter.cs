﻿using UnityEngine;
using System.Collections;

public class TankCharacter : CharacterBase {

	// Use this for initialization
	void Start ()
	{
		MaxHealth = 120;
		Health = Mathf.RoundToInt(MaxHealth);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void IncreasePrimaryStat(float amount)
	{
		this.MaxHealth += amount;
	}
}
