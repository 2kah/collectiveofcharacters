﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class HealthBar : MonoBehaviour {

	public float barDisplay; //current progress
	public Vector2 pos;
	public Vector2 size = new Vector2(30,7);
	public Texture2D emptyTex;
	public Texture2D fullTex;

	IHasHealth owner;

	// Use this for initialization
	void Start ()
	{
		fullTex = new Texture2D((int)size.x,(int)size.y-2);
		FillTexture(fullTex, Color.red);
		fullTex.Apply();

		emptyTex = new Texture2D((int)size.x,(int)size.y);
		FillTexture(emptyTex, new Color(0.1f,0.1f,0.1f,0.5f));
		emptyTex.Apply();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(owner != null)
		{
			barDisplay = (float)owner.GetCurrentHealth() / (float)owner.GetMaxHealth();
		}
	}

	void OnGUI()
	{
		//draw the background:
		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
		GUI.Box(new Rect(0,0, size.x, size.y), emptyTex, new GUIStyle());

		//draw the filled-in part:
		GUI.BeginGroup(new Rect(0,0, size.x * barDisplay, size.y));
		GUI.Box(new Rect(0,1, size.x, size.y), fullTex, new GUIStyle());
		GUI.EndGroup();
		GUI.EndGroup();
	}

	void FillTexture(Texture2D tex, Color colour)
	{
		for(int i = 0; i < tex.width; i++)
		{
			for(int j = 0; j < tex.height; j++)
			{
				tex.SetPixel(i, j, colour);
			}
		}
	}

	public void SetOwner(IHasHealth obj)
	{
		owner = obj;
	}
}
