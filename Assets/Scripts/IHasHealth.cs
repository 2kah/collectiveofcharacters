using System;
namespace AssemblyCSharp
{
	public interface IHasHealth
	{
		int GetCurrentHealth();
		int GetMaxHealth();
	}
}

