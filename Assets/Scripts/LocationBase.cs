﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LocationBase : MonoBehaviour {

	public ObjectiveBase BlueObjective;
	public ObjectiveBase YellowObjective;

	public List<CharacterBase> BlueCharactersPresent;
	public List<CharacterBase> YellowCharactersPresent;

	private Player player;

	// Use this for initialization
	void Start ()
	{
		BlueCharactersPresent = new List<CharacterBase>();
		YellowCharactersPresent = new List<CharacterBase>();
	}

	void OnMouseOver()
	{
		//only register if right mouse button comes up while over the location
		if(Input.GetMouseButtonUp(1))
		{
			player.LocationClicked(this);
		}
	}
	
	// Update is called once per frame
	protected void Update ()
	{
		//hack to ensure player object exists by the time it's needed
		if(player == null)
		{
			player = GameObject.Find ("Player").GetComponent<Player>();
		}

		if(IsAvailable())
		{
			//if characters present, their stats increase
			UpdateCharacterStats();

			//if characters from one team, opponent's objective takes damage and characters take damage
			if(BlueCharactersPresent.Count == 0 && YellowCharactersPresent.Count > 0)
			{

			}

			//if characters from both teams, they fight
		}
	}

	void UpdateCharacterStats()
	{
		foreach(var character in BlueCharactersPresent)
		{
			character.IncreasePrimaryStat(Time.deltaTime / (float)BlueCharactersPresent.Count);
		}

		foreach(var character in YellowCharactersPresent)
		{
			character.IncreasePrimaryStat(Time.deltaTime / (float)YellowCharactersPresent.Count);
		}
	}

	virtual public bool IsAvailable()
	{
		return true;
	}
}
