﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class ObjectiveBase : MonoBehaviour, IHasHealth {

	public bool IsDestroyed = false;
	public bool IsBlue;
	public int Health = 100;
	public readonly int MaxHealth = 100;

	HealthBar healthBar;

	// Use this for initialization
	protected void Initialise ()
	{
		var camera = GameObject.Find("Main Camera").GetComponent<Camera>();
		healthBar = (HealthBar)new GameObject().AddComponent("HealthBar");
		healthBar.SetOwner(this);
		Vector3 pos = camera.WorldToScreenPoint(transform.position);
		float yCoord = Screen.height - pos.y;
		yCoord = IsBlue ? yCoord + 30 : yCoord - 30;
		Vector2 pos2d = new Vector2(pos.x - 14, yCoord);
		healthBar.pos = pos2d;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{

	}

	public int GetCurrentHealth()
	{
		return Health;
	}

	public int GetMaxHealth()
	{
		return MaxHealth;
	}
}
