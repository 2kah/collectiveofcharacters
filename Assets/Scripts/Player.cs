﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	CharacterBase selectedCharacter;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CharacterClicked(CharacterBase character)
	{
		if(selectedCharacter != null) selectedCharacter.SetBackground(false);
		selectedCharacter = character;
		selectedCharacter.SetBackground(true);
	}

	public void LocationClicked(LocationBase location)
	{
		if(selectedCharacter != null && location.IsAvailable())
		{
			selectedCharacter.MoveToLocation(location);
		}
	}
}
