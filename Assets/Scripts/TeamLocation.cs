﻿using UnityEngine;
using System.Collections;
//using AssemblyCSharp;

public class TeamLocation : LocationBase {

	public bool IsBlueLocation;
	public ObjectiveBase[] PreviousObjectives;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	new void Update ()
	{
		base.Update();
		renderer.material.color = IsAvailable() ? Color.white : Color.grey;
	}

	public override bool IsAvailable()
	{
		foreach(var objective in PreviousObjectives)
		{
			if(objective.IsDestroyed) return true;
		}
		return false;
	}
}
